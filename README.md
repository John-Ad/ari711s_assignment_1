#### The data set for part b has not been included in the repo
It can be found here: https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia

Please set the path to the directory in the notebook for b

#### For part B, a bson model was stored on google drive (too big for gitlab). This model was trained on a server and can be used to evaluate the model quickly if the resources required to run the notebook and train the model are not available. There are instructions in the part b note on how to load the model.

https://drive.google.com/file/d/1YmspfZ5CXxWajF_gvKNRkQBzlWntVwxd/view?usp=sharing


